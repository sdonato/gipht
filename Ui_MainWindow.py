# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1506, 834)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralTabWidget = QTabWidget(self.centralwidget)
        self.centralTabWidget.setObjectName(u"centralTabWidget")
        self.centralTabWidget.setGeometry(QRect(10, 10, 1481, 791))
        self.MeasurementTab = QWidget()
        self.MeasurementTab.setObjectName(u"MeasurementTab")
        self.measurementWidget = QWidget(self.MeasurementTab)
        self.measurementWidget.setObjectName(u"measurementWidget")
        self.measurementWidget.setGeometry(QRect(550, 10, 901, 691))
        self.gridLayoutWidget_2 = QWidget(self.measurementWidget)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(10, 10, 871, 671))
        self.MeasurementConfiguration = QGridLayout(self.gridLayoutWidget_2)
        self.MeasurementConfiguration.setObjectName(u"MeasurementConfiguration")
        self.MeasurementConfiguration.setContentsMargins(0, 0, 0, 0)
        self.stopTaskButton = QPushButton(self.gridLayoutWidget_2)
        self.stopTaskButton.setObjectName(u"stopTaskButton")
        self.stopTaskButton.setStyleSheet(u"color:red")

        self.MeasurementConfiguration.addWidget(self.stopTaskButton, 9, 4, 1, 1)

        self.stopRunButton = QPushButton(self.gridLayoutWidget_2)
        self.stopRunButton.setObjectName(u"stopRunButton")
        self.stopRunButton.setStyleSheet(u"color:red")

        self.MeasurementConfiguration.addWidget(self.stopRunButton, 11, 4, 1, 1)

        self.runProgressBar = QProgressBar(self.gridLayoutWidget_2)
        self.runProgressBar.setObjectName(u"runProgressBar")
        self.runProgressBar.setValue(0)

        self.MeasurementConfiguration.addWidget(self.runProgressBar, 11, 1, 1, 3)

        self.taskProgressBar = QProgressBar(self.gridLayoutWidget_2)
        self.taskProgressBar.setObjectName(u"taskProgressBar")
        self.taskProgressBar.setValue(0)

        self.MeasurementConfiguration.addWidget(self.taskProgressBar, 9, 1, 1, 3)

        self.runProgressBarLabel = QLabel(self.gridLayoutWidget_2)
        self.runProgressBarLabel.setObjectName(u"runProgressBarLabel")

        self.MeasurementConfiguration.addWidget(self.runProgressBarLabel, 11, 0, 1, 1)

        self.taskProgressBarLabel = QLabel(self.gridLayoutWidget_2)
        self.taskProgressBarLabel.setObjectName(u"taskProgressBarLabel")

        self.MeasurementConfiguration.addWidget(self.taskProgressBarLabel, 9, 0, 1, 1)

        self.startMeasurementButton = QPushButton(self.gridLayoutWidget_2)
        self.startMeasurementButton.setObjectName(u"startMeasurementButton")
        self.startMeasurementButton.setStyleSheet(u"color:green")

        self.MeasurementConfiguration.addWidget(self.startMeasurementButton, 7, 4, 1, 1)

        self.label = QLabel(self.gridLayoutWidget_2)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(0, 35))

        self.MeasurementConfiguration.addWidget(self.label, 0, 0, 1, 1)

        self.taskListLayout = QVBoxLayout()
        self.taskListLayout.setObjectName(u"taskListLayout")
        self.taskTreeWidget = QTreeWidget(self.gridLayoutWidget_2)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.taskTreeWidget.setHeaderItem(__qtreewidgetitem)
        self.taskTreeWidget.setObjectName(u"taskTreeWidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.taskTreeWidget.sizePolicy().hasHeightForWidth())
        self.taskTreeWidget.setSizePolicy(sizePolicy1)

        self.taskListLayout.addWidget(self.taskTreeWidget)


        self.MeasurementConfiguration.addLayout(self.taskListLayout, 6, 1, 1, 4)

        self.slotLayout = QVBoxLayout()
        self.slotLayout.setObjectName(u"slotLayout")

        self.MeasurementConfiguration.addLayout(self.slotLayout, 3, 0, 1, 5)

        self.label_2 = QLabel(self.gridLayoutWidget_2)
        self.label_2.setObjectName(u"label_2")
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QSize(0, 35))

        self.MeasurementConfiguration.addWidget(self.label_2, 1, 0, 1, 1)

        self.measurementCheckBoxes = QHBoxLayout()
        self.measurementCheckBoxes.setObjectName(u"measurementCheckBoxes")
        self.measurementCheckBoxes.setSizeConstraint(QLayout.SetMinimumSize)
        self.measurementCheckBoxes.setContentsMargins(-1, -1, -1, 10)

        self.MeasurementConfiguration.addLayout(self.measurementCheckBoxes, 0, 2, 1, 3)

        self.moduleTestLayout = QHBoxLayout()
        self.moduleTestLayout.setObjectName(u"moduleTestLayout")
        self.nEventsLabel = QLabel(self.gridLayoutWidget_2)
        self.nEventsLabel.setObjectName(u"nEventsLabel")
        self.nEventsLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.moduleTestLayout.addWidget(self.nEventsLabel)

        self.nEventsLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.nEventsLineEdit.setObjectName(u"nEventsLineEdit")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.nEventsLineEdit.sizePolicy().hasHeightForWidth())
        self.nEventsLineEdit.setSizePolicy(sizePolicy2)

        self.moduleTestLayout.addWidget(self.nEventsLineEdit)

        self.latencyLabel = QLabel(self.gridLayoutWidget_2)
        self.latencyLabel.setObjectName(u"latencyLabel")
        self.latencyLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.moduleTestLayout.addWidget(self.latencyLabel)

        self.latencyLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.latencyLineEdit.setObjectName(u"latencyLineEdit")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.latencyLineEdit.sizePolicy().hasHeightForWidth())
        self.latencyLineEdit.setSizePolicy(sizePolicy3)
        self.latencyLineEdit.setMaximumSize(QSize(50, 16777215))

        self.moduleTestLayout.addWidget(self.latencyLineEdit)

        self.thresholdLabel = QLabel(self.gridLayoutWidget_2)
        self.thresholdLabel.setObjectName(u"thresholdLabel")
        self.thresholdLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.moduleTestLayout.addWidget(self.thresholdLabel)

        self.thresholdLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.thresholdLineEdit.setObjectName(u"thresholdLineEdit")
        sizePolicy3.setHeightForWidth(self.thresholdLineEdit.sizePolicy().hasHeightForWidth())
        self.thresholdLineEdit.setSizePolicy(sizePolicy3)
        self.thresholdLineEdit.setMaximumSize(QSize(50, 16777215))

        self.moduleTestLayout.addWidget(self.thresholdLineEdit)

        self.extTriggerCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.extTriggerCheckBox.setObjectName(u"extTriggerCheckBox")

        self.moduleTestLayout.addWidget(self.extTriggerCheckBox)


        self.MeasurementConfiguration.addLayout(self.moduleTestLayout, 1, 2, 1, 3)

        self.fc7LogSettingsWidget = QWidget(self.MeasurementTab)
        self.fc7LogSettingsWidget.setObjectName(u"fc7LogSettingsWidget")
        self.fc7LogSettingsWidget.setGeometry(QRect(10, 100, 541, 601))
        self.consoleOutput = QTextEdit(self.fc7LogSettingsWidget)
        self.consoleOutput.setObjectName(u"consoleOutput")
        self.consoleOutput.setGeometry(QRect(10, 149, 521, 441))
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.consoleOutput.sizePolicy().hasHeightForWidth())
        self.consoleOutput.setSizePolicy(sizePolicy4)
        self.consoleOutput.setMinimumSize(QSize(0, 150))
        font = QFont()
        font.setPointSize(7)
        font.setBold(False)
        font.setWeight(50)
        self.consoleOutput.setFont(font)
        self.consoleOutput.setAutoFillBackground(False)
        self.consoleOutput.setStyleSheet(u"QTextEdit{\n"
"background-color: rgb(46, 52, 54);\n"
"color: rgb(255, 255, 255);\n"
"}")
        self.consoleOutput.setReadOnly(True)
        self.gridLayoutWidget_5 = QWidget(self.fc7LogSettingsWidget)
        self.gridLayoutWidget_5.setObjectName(u"gridLayoutWidget_5")
        self.gridLayoutWidget_5.setGeometry(QRect(10, 10, 521, 113))
        self.GeneralInformationGrid = QGridLayout(self.gridLayoutWidget_5)
        self.GeneralInformationGrid.setObjectName(u"GeneralInformationGrid")
        self.GeneralInformationGrid.setContentsMargins(0, 0, 0, 0)
        self.operatorLabel = QLabel(self.gridLayoutWidget_5)
        self.operatorLabel.setObjectName(u"operatorLabel")
        font1 = QFont()
        font1.setPointSize(15)
        self.operatorLabel.setFont(font1)

        self.GeneralInformationGrid.addWidget(self.operatorLabel, 0, 0, 1, 1)

        self.locationLabel = QLabel(self.gridLayoutWidget_5)
        self.locationLabel.setObjectName(u"locationLabel")
        self.locationLabel.setFont(font1)

        self.GeneralInformationGrid.addWidget(self.locationLabel, 1, 0, 1, 1)

        self.locationComboBox = QComboBox(self.gridLayoutWidget_5)
        self.locationComboBox.setObjectName(u"locationComboBox")

        self.GeneralInformationGrid.addWidget(self.locationComboBox, 1, 2, 1, 2)

        self.addOperatorButton = QPushButton(self.gridLayoutWidget_5)
        self.addOperatorButton.setObjectName(u"addOperatorButton")

        self.GeneralInformationGrid.addWidget(self.addOperatorButton, 0, 1, 1, 1)

        self.operatorComboBox = QComboBox(self.gridLayoutWidget_5)
        self.operatorComboBox.setObjectName(u"operatorComboBox")

        self.GeneralInformationGrid.addWidget(self.operatorComboBox, 0, 2, 1, 2)

        self.dbStatusLabel_2 = QLabel(self.gridLayoutWidget_5)
        self.dbStatusLabel_2.setObjectName(u"dbStatusLabel_2")
        self.dbStatusLabel_2.setFont(font1)

        self.GeneralInformationGrid.addWidget(self.dbStatusLabel_2, 2, 0, 1, 1)

        self.dbStatusDisplayLabel = QLabel(self.gridLayoutWidget_5)
        self.dbStatusDisplayLabel.setObjectName(u"dbStatusDisplayLabel")
        self.dbStatusDisplayLabel.setFont(font1)

        self.GeneralInformationGrid.addWidget(self.dbStatusDisplayLabel, 2, 1, 1, 1)

        self.GiphtIcon = QLabel(self.MeasurementTab)
        self.GiphtIcon.setObjectName(u"GiphtIcon")
        self.GiphtIcon.setGeometry(QRect(80, 0, 100, 100))
        self.GiphtIcon.setPixmap(QPixmap(u"Utils/gift.png"))
        self.GiphtIcon.setScaledContents(True)
        self.GiphtIcon.setAlignment(Qt.AlignCenter)
        self.GiphtIcon.setMargin(5)
        self.gridLayoutWidget_6 = QWidget(self.MeasurementTab)
        self.gridLayoutWidget_6.setObjectName(u"gridLayoutWidget_6")
        self.gridLayoutWidget_6.setGeometry(QRect(230, 20, 311, 80))
        self.runSlotsGridLayout = QGridLayout(self.gridLayoutWidget_6)
        self.runSlotsGridLayout.setObjectName(u"runSlotsGridLayout")
        self.runSlotsGridLayout.setContentsMargins(0, 0, 0, 0)
        self.runNumberLabel = QLabel(self.gridLayoutWidget_6)
        self.runNumberLabel.setObjectName(u"runNumberLabel")

        self.runSlotsGridLayout.addWidget(self.runNumberLabel, 1, 0, 1, 1)

        self.slotsLabel = QLabel(self.gridLayoutWidget_6)
        self.slotsLabel.setObjectName(u"slotsLabel")

        self.runSlotsGridLayout.addWidget(self.slotsLabel, 3, 0, 1, 1)

        self.slotsSpinBox = QSpinBox(self.gridLayoutWidget_6)
        self.slotsSpinBox.setObjectName(u"slotsSpinBox")

        self.runSlotsGridLayout.addWidget(self.slotsSpinBox, 3, 1, 1, 1)

        self.runNumberLabelValue = QLabel(self.gridLayoutWidget_6)
        self.runNumberLabelValue.setObjectName(u"runNumberLabelValue")

        self.runSlotsGridLayout.addWidget(self.runNumberLabelValue, 1, 1, 1, 1)

        self.configureSlotsButton = QPushButton(self.gridLayoutWidget_6)
        self.configureSlotsButton.setObjectName(u"configureSlotsButton")

        self.runSlotsGridLayout.addWidget(self.configureSlotsButton, 3, 2, 1, 1)

        self.centralTabWidget.addTab(self.MeasurementTab, "")
        self.ResultsTab = QWidget()
        self.ResultsTab.setObjectName(u"ResultsTab")
        self.resultsTreeWidget = QTreeWidget(self.ResultsTab)
        __qtreewidgetitem1 = QTreeWidgetItem()
        __qtreewidgetitem1.setText(0, u"1");
        self.resultsTreeWidget.setHeaderItem(__qtreewidgetitem1)
        self.resultsTreeWidget.setObjectName(u"resultsTreeWidget")
        self.resultsTreeWidget.setGeometry(QRect(10, 50, 1461, 681))
        self.convertAllResultsButton = QPushButton(self.ResultsTab)
        self.convertAllResultsButton.setObjectName(u"convertAllResultsButton")
        self.convertAllResultsButton.setGeometry(QRect(1260, 10, 101, 36))
        self.uploadAllResultsButton = QPushButton(self.ResultsTab)
        self.uploadAllResultsButton.setObjectName(u"uploadAllResultsButton")
        self.uploadAllResultsButton.setGeometry(QRect(1370, 10, 101, 36))
        self.loadResultFolderButton = QPushButton(self.ResultsTab)
        self.loadResultFolderButton.setObjectName(u"loadResultFolderButton")
        self.loadResultFolderButton.setGeometry(QRect(410, 10, 141, 36))
        self.uploadResultsButton = QPushButton(self.ResultsTab)
        self.uploadResultsButton.setObjectName(u"uploadResultsButton")
        self.uploadResultsButton.setGeometry(QRect(100, 10, 81, 36))
        self.convertResultsButton = QPushButton(self.ResultsTab)
        self.convertResultsButton.setObjectName(u"convertResultsButton")
        self.convertResultsButton.setGeometry(QRect(10, 10, 81, 36))
        self.centralTabWidget.addTab(self.ResultsTab, "")
        self.ConfigurationTab = QWidget()
        self.ConfigurationTab.setObjectName(u"ConfigurationTab")
        self.gridLayoutWidget = QWidget(self.ConfigurationTab)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 889, 383))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.ph2AcfLabel_2 = QLabel(self.gridLayoutWidget)
        self.ph2AcfLabel_2.setObjectName(u"ph2AcfLabel_2")

        self.gridLayout.addWidget(self.ph2AcfLabel_2, 1, 0, 1, 1)

        self.openDialogPh2ACFFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogPh2ACFFolderButton.setObjectName(u"openDialogPh2ACFFolderButton")
        icon = QIcon()
        icon.addFile(u"Utils/folder.jpg", QSize(), QIcon.Normal, QIcon.Off)
        self.openDialogPh2ACFFolderButton.setIcon(icon)
        self.openDialogPh2ACFFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogPh2ACFFolderButton, 0, 2, 1, 1)

        self.checkPh2Acf = QPushButton(self.gridLayoutWidget)
        self.checkPh2Acf.setObjectName(u"checkPh2Acf")
        self.checkPh2Acf.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.checkPh2Acf, 0, 3, 1, 1)

        self.openDefault2SXmlButton = QPushButton(self.gridLayoutWidget)
        self.openDefault2SXmlButton.setObjectName(u"openDefault2SXmlButton")
        self.openDefault2SXmlButton.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.openDefault2SXmlButton, 2, 3, 1, 1)

        self.openDialogResultsFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogResultsFolderButton.setObjectName(u"openDialogResultsFolderButton")
        self.openDialogResultsFolderButton.setIcon(icon)
        self.openDialogResultsFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogResultsFolderButton, 4, 2, 1, 1)

        self.openDialogDevicePackageFolderButton = QPushButton(self.gridLayoutWidget)
        self.openDialogDevicePackageFolderButton.setObjectName(u"openDialogDevicePackageFolderButton")
        self.openDialogDevicePackageFolderButton.setIcon(icon)
        self.openDialogDevicePackageFolderButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogDevicePackageFolderButton, 1, 2, 1, 1)

        self.defaultPSXmlLabel = QLabel(self.gridLayoutWidget)
        self.defaultPSXmlLabel.setObjectName(u"defaultPSXmlLabel")

        self.gridLayout.addWidget(self.defaultPSXmlLabel, 3, 0, 1, 1)

        self.checkControlhub = QPushButton(self.gridLayoutWidget)
        self.checkControlhub.setObjectName(u"checkControlhub")
        sizePolicy.setHeightForWidth(self.checkControlhub.sizePolicy().hasHeightForWidth())
        self.checkControlhub.setSizePolicy(sizePolicy)
        self.checkControlhub.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.checkControlhub, 0, 4, 1, 1)

        self.openDialogDefault2SXmlFileButton = QPushButton(self.gridLayoutWidget)
        self.openDialogDefault2SXmlFileButton.setObjectName(u"openDialogDefault2SXmlFileButton")
        self.openDialogDefault2SXmlFileButton.setIcon(icon)
        self.openDialogDefault2SXmlFileButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogDefault2SXmlFileButton, 2, 2, 1, 1)

        self.restartDeviceButton = QPushButton(self.gridLayoutWidget)
        self.restartDeviceButton.setObjectName(u"restartDeviceButton")

        self.gridLayout.addWidget(self.restartDeviceButton, 5, 4, 1, 1)

        self.defaultPSXmlLineEdit = QLineEdit(self.gridLayoutWidget)
        self.defaultPSXmlLineEdit.setObjectName(u"defaultPSXmlLineEdit")

        self.gridLayout.addWidget(self.defaultPSXmlLineEdit, 3, 1, 1, 1)

        self.openDialogDefaultPSXmlFileButton = QPushButton(self.gridLayoutWidget)
        self.openDialogDefaultPSXmlFileButton.setObjectName(u"openDialogDefaultPSXmlFileButton")
        self.openDialogDefaultPSXmlFileButton.setIcon(icon)
        self.openDialogDefaultPSXmlFileButton.setIconSize(QSize(24, 24))

        self.gridLayout.addWidget(self.openDialogDefaultPSXmlFileButton, 3, 2, 1, 1)

        self.ph2AcfLabel = QLabel(self.gridLayoutWidget)
        self.ph2AcfLabel.setObjectName(u"ph2AcfLabel")

        self.gridLayout.addWidget(self.ph2AcfLabel, 0, 0, 1, 1)

        self.default2SXmlLabel = QLabel(self.gridLayoutWidget)
        self.default2SXmlLabel.setObjectName(u"default2SXmlLabel")

        self.gridLayout.addWidget(self.default2SXmlLabel, 2, 0, 1, 1)

        self.ph2AcfFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.ph2AcfFolderLineEdit.setObjectName(u"ph2AcfFolderLineEdit")

        self.gridLayout.addWidget(self.ph2AcfFolderLineEdit, 0, 1, 1, 1)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.invertPlotCheckBox = QCheckBox(self.gridLayoutWidget)
        self.invertPlotCheckBox.setObjectName(u"invertPlotCheckBox")
        sizePolicy2.setHeightForWidth(self.invertPlotCheckBox.sizePolicy().hasHeightForWidth())
        self.invertPlotCheckBox.setSizePolicy(sizePolicy2)

        self.gridLayout_2.addWidget(self.invertPlotCheckBox, 0, 1, 1, 1)

        self.powerSupplyDialogCheckBox = QCheckBox(self.gridLayoutWidget)
        self.powerSupplyDialogCheckBox.setObjectName(u"powerSupplyDialogCheckBox")
        sizePolicy2.setHeightForWidth(self.powerSupplyDialogCheckBox.sizePolicy().hasHeightForWidth())
        self.powerSupplyDialogCheckBox.setSizePolicy(sizePolicy2)
        self.powerSupplyDialogCheckBox.setMinimumSize(QSize(0, 0))

        self.gridLayout_2.addWidget(self.powerSupplyDialogCheckBox, 1, 1, 1, 1)

        self.showPlotCheckBox = QCheckBox(self.gridLayoutWidget)
        self.showPlotCheckBox.setObjectName(u"showPlotCheckBox")
        sizePolicy2.setHeightForWidth(self.showPlotCheckBox.sizePolicy().hasHeightForWidth())
        self.showPlotCheckBox.setSizePolicy(sizePolicy2)

        self.gridLayout_2.addWidget(self.showPlotCheckBox, 0, 0, 1, 1)

        self.groupBoxLpGbt = QGroupBox(self.gridLayoutWidget)
        self.groupBoxLpGbt.setObjectName(u"groupBoxLpGbt")
        self.groupBoxLpGbt.setAutoFillBackground(False)
        self.groupBoxLpGbt.setStyleSheet(u"")
        self.groupBoxLpGbt.setCheckable(False)
        self.lpGBTv0RadioButton = QRadioButton(self.groupBoxLpGbt)
        self.lpGBTv0RadioButton.setObjectName(u"lpGBTv0RadioButton")
        self.lpGBTv0RadioButton.setEnabled(True)
        self.lpGBTv0RadioButton.setGeometry(QRect(5, 5, 85, 26))
        self.lpGBTv0RadioButton.setAutoExclusive(True)
        self.lpGBTv1RadioButton = QRadioButton(self.groupBoxLpGbt)
        self.lpGBTv1RadioButton.setObjectName(u"lpGBTv1RadioButton")
        self.lpGBTv1RadioButton.setGeometry(QRect(5, 35, 85, 21))
        self.lpGBTv1RadioButton.setAutoExclusive(True)

        self.gridLayout_2.addWidget(self.groupBoxLpGbt, 0, 3, 2, 1)

        self.autoCheckStartCheckBox = QCheckBox(self.gridLayoutWidget)
        self.autoCheckStartCheckBox.setObjectName(u"autoCheckStartCheckBox")
        sizePolicy2.setHeightForWidth(self.autoCheckStartCheckBox.sizePolicy().hasHeightForWidth())
        self.autoCheckStartCheckBox.setSizePolicy(sizePolicy2)

        self.gridLayout_2.addWidget(self.autoCheckStartCheckBox, 1, 0, 1, 1)

        self.groupBoxCic = QGroupBox(self.gridLayoutWidget)
        self.groupBoxCic.setObjectName(u"groupBoxCic")
        sizePolicy5 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.groupBoxCic.sizePolicy().hasHeightForWidth())
        self.groupBoxCic.setSizePolicy(sizePolicy5)
        self.groupBoxCic.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.groupBoxCic.setFlat(False)
        self.groupBoxCic.setCheckable(False)
        self.cic2RadioButton = QRadioButton(self.groupBoxCic)
        self.cic2RadioButton.setObjectName(u"cic2RadioButton")
        self.cic2RadioButton.setGeometry(QRect(5, 35, 60, 26))
        self.cic2RadioButton.setAutoExclusive(True)
        self.cic1RadioButton = QRadioButton(self.groupBoxCic)
        self.cic1RadioButton.setObjectName(u"cic1RadioButton")
        self.cic1RadioButton.setGeometry(QRect(5, 5, 60, 26))
        self.cic1RadioButton.setAutoExclusive(True)

        self.gridLayout_2.addWidget(self.groupBoxCic, 0, 2, 2, 1)


        self.gridLayout.addLayout(self.gridLayout_2, 5, 0, 1, 3)

        self.resultFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.resultFolderLineEdit.setObjectName(u"resultFolderLineEdit")

        self.gridLayout.addWidget(self.resultFolderLineEdit, 4, 1, 1, 1)

        self.default2SXmlLineEdit = QLineEdit(self.gridLayoutWidget)
        self.default2SXmlLineEdit.setObjectName(u"default2SXmlLineEdit")

        self.gridLayout.addWidget(self.default2SXmlLineEdit, 2, 1, 1, 1)

        self.resultsFolderLabel = QLabel(self.gridLayoutWidget)
        self.resultsFolderLabel.setObjectName(u"resultsFolderLabel")

        self.gridLayout.addWidget(self.resultsFolderLabel, 4, 0, 1, 1)

        self.devicePackageFolderLineEdit = QLineEdit(self.gridLayoutWidget)
        self.devicePackageFolderLineEdit.setObjectName(u"devicePackageFolderLineEdit")

        self.gridLayout.addWidget(self.devicePackageFolderLineEdit, 1, 1, 1, 1)

        self.openDefaultPSXmlButton = QPushButton(self.gridLayoutWidget)
        self.openDefaultPSXmlButton.setObjectName(u"openDefaultPSXmlButton")
        self.openDefaultPSXmlButton.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.openDefaultPSXmlButton, 3, 3, 1, 1)

        self.startControlhubButton = QPushButton(self.gridLayoutWidget)
        self.startControlhubButton.setObjectName(u"startControlhubButton")
        sizePolicy.setHeightForWidth(self.startControlhubButton.sizePolicy().hasHeightForWidth())
        self.startControlhubButton.setSizePolicy(sizePolicy)
        self.startControlhubButton.setMinimumSize(QSize(115, 0))

        self.gridLayout.addWidget(self.startControlhubButton, 1, 4, 1, 1)

        self.gridLayoutWidget_7 = QWidget(self.ConfigurationTab)
        self.gridLayoutWidget_7.setObjectName(u"gridLayoutWidget_7")
        self.gridLayoutWidget_7.setGeometry(QRect(10, 400, 881, 341))
        self.devicesControlGrid = QGridLayout(self.gridLayoutWidget_7)
        self.devicesControlGrid.setObjectName(u"devicesControlGrid")
        self.devicesControlGrid.setContentsMargins(0, 0, 0, 0)
        self.addFc7Button = QPushButton(self.gridLayoutWidget_7)
        self.addFc7Button.setObjectName(u"addFc7Button")

        self.devicesControlGrid.addWidget(self.addFc7Button, 1, 0, 1, 1)

        self.removeDeviceButton = QPushButton(self.gridLayoutWidget_7)
        self.removeDeviceButton.setObjectName(u"removeDeviceButton")

        self.devicesControlGrid.addWidget(self.removeDeviceButton, 4, 0, 1, 1)

        self.addPowerSupplyButton = QPushButton(self.gridLayoutWidget_7)
        self.addPowerSupplyButton.setObjectName(u"addPowerSupplyButton")

        self.devicesControlGrid.addWidget(self.addPowerSupplyButton, 2, 0, 1, 1)

        self.addArduinoButton = QPushButton(self.gridLayoutWidget_7)
        self.addArduinoButton.setObjectName(u"addArduinoButton")

        self.devicesControlGrid.addWidget(self.addArduinoButton, 3, 0, 1, 1)

        self.GiphtIcon_2 = QLabel(self.gridLayoutWidget_7)
        self.GiphtIcon_2.setObjectName(u"GiphtIcon_2")
        self.GiphtIcon_2.setMaximumSize(QSize(100, 100))
        self.GiphtIcon_2.setPixmap(QPixmap(u"Utils/gift.png"))
        self.GiphtIcon_2.setScaledContents(True)
        self.GiphtIcon_2.setAlignment(Qt.AlignCenter)
        self.GiphtIcon_2.setMargin(5)

        self.devicesControlGrid.addWidget(self.GiphtIcon_2, 6, 0, 1, 1)

        self.deviceTreeWidget = QTreeWidget(self.gridLayoutWidget_7)
        __qtreewidgetitem2 = QTreeWidgetItem()
        __qtreewidgetitem2.setText(0, u"1");
        self.deviceTreeWidget.setHeaderItem(__qtreewidgetitem2)
        self.deviceTreeWidget.setObjectName(u"deviceTreeWidget")

        self.devicesControlGrid.addWidget(self.deviceTreeWidget, 1, 1, 6, 1)

        self.verticalLayoutWidget = QWidget(self.ConfigurationTab)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(900, 10, 571, 731))
        self.deviceConfigsLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.deviceConfigsLayout.setObjectName(u"deviceConfigsLayout")
        self.deviceConfigsLayout.setContentsMargins(0, 0, 0, 0)
        self.centralTabWidget.addTab(self.ConfigurationTab, "")
        self.MonitorTab = QWidget()
        self.MonitorTab.setObjectName(u"MonitorTab")
        self.gridLayoutWidget_8 = QWidget(self.MonitorTab)
        self.gridLayoutWidget_8.setObjectName(u"gridLayoutWidget_8")
        self.gridLayoutWidget_8.setGeometry(QRect(9, 9, 1451, 711))
        self.monitorLayout = QGridLayout(self.gridLayoutWidget_8)
        self.monitorLayout.setObjectName(u"monitorLayout")
        self.monitorLayout.setContentsMargins(0, 0, 0, 0)
        self.centralTabWidget.addTab(self.MonitorTab, "")
        self.ExpertTab = QWidget()
        self.ExpertTab.setObjectName(u"ExpertTab")
        self.gridLayoutWidget_3 = QWidget(self.ExpertTab)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(10, 10, 541, 203))
        self.dbSettingsGrid = QGridLayout(self.gridLayoutWidget_3)
        self.dbSettingsGrid.setObjectName(u"dbSettingsGrid")
        self.dbSettingsGrid.setContentsMargins(0, 0, 0, 0)
        self.baserUrlLabel = QLabel(self.gridLayoutWidget_3)
        self.baserUrlLabel.setObjectName(u"baserUrlLabel")

        self.dbSettingsGrid.addWidget(self.baserUrlLabel, 4, 0, 1, 1)

        self.dbStatusDisplayLabel_2 = QLabel(self.gridLayoutWidget_3)
        self.dbStatusDisplayLabel_2.setObjectName(u"dbStatusDisplayLabel_2")

        self.dbSettingsGrid.addWidget(self.dbStatusDisplayLabel_2, 0, 1, 1, 1)

        self.dbStatusLabelName = QLabel(self.gridLayoutWidget_3)
        self.dbStatusLabelName.setObjectName(u"dbStatusLabelName")

        self.dbSettingsGrid.addWidget(self.dbStatusLabelName, 0, 0, 1, 1)

        self.initDbHandlerOnStartupCheckBox = QCheckBox(self.gridLayoutWidget_3)
        self.initDbHandlerOnStartupCheckBox.setObjectName(u"initDbHandlerOnStartupCheckBox")

        self.dbSettingsGrid.addWidget(self.initDbHandlerOnStartupCheckBox, 3, 1, 1, 1)

        self.useDbToCheckModules = QLabel(self.gridLayoutWidget_3)
        self.useDbToCheckModules.setObjectName(u"useDbToCheckModules")

        self.dbSettingsGrid.addWidget(self.useDbToCheckModules, 2, 0, 1, 1)

        self.uploadUrlLineEdit = QLineEdit(self.gridLayoutWidget_3)
        self.uploadUrlLineEdit.setObjectName(u"uploadUrlLineEdit")

        self.dbSettingsGrid.addWidget(self.uploadUrlLineEdit, 5, 1, 1, 1)

        self.useDbToCheckModulesButton = QCheckBox(self.gridLayoutWidget_3)
        self.useDbToCheckModulesButton.setObjectName(u"useDbToCheckModulesButton")

        self.dbSettingsGrid.addWidget(self.useDbToCheckModulesButton, 2, 1, 1, 1)

        self.baseUrlLineEdit = QLineEdit(self.gridLayoutWidget_3)
        self.baseUrlLineEdit.setObjectName(u"baseUrlLineEdit")

        self.dbSettingsGrid.addWidget(self.baseUrlLineEdit, 4, 1, 1, 1)

        self.uploadUrlLabel = QLabel(self.gridLayoutWidget_3)
        self.uploadUrlLabel.setObjectName(u"uploadUrlLabel")

        self.dbSettingsGrid.addWidget(self.uploadUrlLabel, 5, 0, 1, 1)

        self.initDbStartupLabel = QLabel(self.gridLayoutWidget_3)
        self.initDbStartupLabel.setObjectName(u"initDbStartupLabel")

        self.dbSettingsGrid.addWidget(self.initDbStartupLabel, 3, 0, 1, 1)

        self.checkDbButton = QPushButton(self.gridLayoutWidget_3)
        self.checkDbButton.setObjectName(u"checkDbButton")

        self.dbSettingsGrid.addWidget(self.checkDbButton, 1, 0, 1, 1)

        self.testButton = QPushButton(self.ExpertTab)
        self.testButton.setObjectName(u"testButton")
        self.testButton.setGeometry(QRect(820, 10, 101, 41))
        self.expertModeCheckBox = QCheckBox(self.ExpertTab)
        self.expertModeCheckBox.setObjectName(u"expertModeCheckBox")
        self.expertModeCheckBox.setGeometry(QRect(610, 20, 151, 20))
        self.gridLayoutWidget_4 = QWidget(self.ExpertTab)
        self.gridLayoutWidget_4.setObjectName(u"gridLayoutWidget_4")
        self.gridLayoutWidget_4.setGeometry(QRect(590, 60, 358, 384))
        self.moduleTestSettngsLayout = QGridLayout(self.gridLayoutWidget_4)
        self.moduleTestSettngsLayout.setObjectName(u"moduleTestSettngsLayout")
        self.moduleTestSettngsLayout.setContentsMargins(0, 0, 0, 0)
        self.ivReadoutsLabel_3 = QLabel(self.gridLayoutWidget_4)
        self.ivReadoutsLabel_3.setObjectName(u"ivReadoutsLabel_3")

        self.moduleTestSettngsLayout.addWidget(self.ivReadoutsLabel_3, 3, 0, 1, 1)

        self.vSettingsLabel_2 = QLabel(self.gridLayoutWidget_4)
        self.vSettingsLabel_2.setObjectName(u"vSettingsLabel_2")
        sizePolicy6 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.vSettingsLabel_2.sizePolicy().hasHeightForWidth())
        self.vSettingsLabel_2.setSizePolicy(sizePolicy6)
        self.vSettingsLabel_2.setMinimumSize(QSize(0, 40))
        self.vSettingsLabel_2.setMaximumSize(QSize(16777215, 40))

        self.moduleTestSettngsLayout.addWidget(self.vSettingsLabel_2, 0, 0, 1, 1)

        self.ivReadoutsLabel_4 = QLabel(self.gridLayoutWidget_4)
        self.ivReadoutsLabel_4.setObjectName(u"ivReadoutsLabel_4")

        self.moduleTestSettngsLayout.addWidget(self.ivReadoutsLabel_4, 4, 0, 1, 1)

        self.ivReadoutsLabel = QLabel(self.gridLayoutWidget_4)
        self.ivReadoutsLabel.setObjectName(u"ivReadoutsLabel")

        self.moduleTestSettngsLayout.addWidget(self.ivReadoutsLabel, 1, 0, 1, 1)

        self.ivReadoutsLabel_5 = QLabel(self.gridLayoutWidget_4)
        self.ivReadoutsLabel_5.setObjectName(u"ivReadoutsLabel_5")

        self.moduleTestSettngsLayout.addWidget(self.ivReadoutsLabel_5, 5, 0, 1, 1)

        self.ivReadoutsLabel_2 = QLabel(self.gridLayoutWidget_4)
        self.ivReadoutsLabel_2.setObjectName(u"ivReadoutsLabel_2")

        self.moduleTestSettngsLayout.addWidget(self.ivReadoutsLabel_2, 2, 0, 1, 1)

        self.HVTestLabel = QLabel(self.gridLayoutWidget_4)
        self.HVTestLabel.setObjectName(u"HVTestLabel")
        self.HVTestLabel.setMaximumSize(QSize(350, 16777215))
        self.HVTestLabel.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.moduleTestSettngsLayout.addWidget(self.HVTestLabel, 7, 0, 1, 1)

        self.vtrxLightOffCheckBox = QCheckBox(self.gridLayoutWidget_4)
        self.vtrxLightOffCheckBox.setObjectName(u"vtrxLightOffCheckBox")

        self.moduleTestSettngsLayout.addWidget(self.vtrxLightOffCheckBox, 0, 1, 1, 1)

        self.LVTestLabel = QLabel(self.gridLayoutWidget_4)
        self.LVTestLabel.setObjectName(u"LVTestLabel")
        self.LVTestLabel.setMinimumSize(QSize(40, 0))
        self.LVTestLabel.setMaximumSize(QSize(350, 16777215))
        self.LVTestLabel.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.moduleTestSettngsLayout.addWidget(self.LVTestLabel, 6, 0, 1, 1)

        self.HVTestLabel_2 = QLabel(self.gridLayoutWidget_4)
        self.HVTestLabel_2.setObjectName(u"HVTestLabel_2")
        sizePolicy5.setHeightForWidth(self.HVTestLabel_2.sizePolicy().hasHeightForWidth())
        self.HVTestLabel_2.setSizePolicy(sizePolicy5)
        self.HVTestLabel_2.setMaximumSize(QSize(350, 16777215))
        self.HVTestLabel_2.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.moduleTestSettngsLayout.addWidget(self.HVTestLabel_2, 8, 0, 1, 1)

        self.readoutsSpinBox = QSpinBox(self.gridLayoutWidget_4)
        self.readoutsSpinBox.setObjectName(u"readoutsSpinBox")

        self.moduleTestSettngsLayout.addWidget(self.readoutsSpinBox, 1, 1, 1, 1)

        self.settlingTimeLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.settlingTimeLineEdit.setObjectName(u"settlingTimeLineEdit")

        self.moduleTestSettngsLayout.addWidget(self.settlingTimeLineEdit, 2, 1, 1, 1)

        self.vStepLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.vStepLineEdit.setObjectName(u"vStepLineEdit")

        self.moduleTestSettngsLayout.addWidget(self.vStepLineEdit, 3, 1, 1, 1)

        self.vMaxUnencapsulatedLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.vMaxUnencapsulatedLineEdit.setObjectName(u"vMaxUnencapsulatedLineEdit")
        sizePolicy7 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.vMaxUnencapsulatedLineEdit.sizePolicy().hasHeightForWidth())
        self.vMaxUnencapsulatedLineEdit.setSizePolicy(sizePolicy7)

        self.moduleTestSettngsLayout.addWidget(self.vMaxUnencapsulatedLineEdit, 4, 1, 1, 1)

        self.vMaxEncapsulatedLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.vMaxEncapsulatedLineEdit.setObjectName(u"vMaxEncapsulatedLineEdit")
        sizePolicy7.setHeightForWidth(self.vMaxEncapsulatedLineEdit.sizePolicy().hasHeightForWidth())
        self.vMaxEncapsulatedLineEdit.setSizePolicy(sizePolicy7)

        self.moduleTestSettngsLayout.addWidget(self.vMaxEncapsulatedLineEdit, 5, 1, 1, 1)

        self.LVModuleTestLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.LVModuleTestLineEdit.setObjectName(u"LVModuleTestLineEdit")
        self.LVModuleTestLineEdit.setMaximumSize(QSize(16777215, 16777215))

        self.moduleTestSettngsLayout.addWidget(self.LVModuleTestLineEdit, 6, 1, 1, 1)

        self.HVModuleTestUnencapsulatedLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.HVModuleTestUnencapsulatedLineEdit.setObjectName(u"HVModuleTestUnencapsulatedLineEdit")
        self.HVModuleTestUnencapsulatedLineEdit.setMaximumSize(QSize(16777215, 16777215))

        self.moduleTestSettngsLayout.addWidget(self.HVModuleTestUnencapsulatedLineEdit, 7, 1, 1, 1)

        self.HVModuleTestEncapsulatedLineEdit = QLineEdit(self.gridLayoutWidget_4)
        self.HVModuleTestEncapsulatedLineEdit.setObjectName(u"HVModuleTestEncapsulatedLineEdit")
        self.HVModuleTestEncapsulatedLineEdit.setMaximumSize(QSize(16777215, 16777215))

        self.moduleTestSettngsLayout.addWidget(self.HVModuleTestEncapsulatedLineEdit, 8, 1, 1, 1)

        self.centralTabWidget.addTab(self.ExpertTab, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.centralTabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.stopTaskButton.setText(QCoreApplication.translate("MainWindow", u"STOP Task", None))
        self.stopRunButton.setText(QCoreApplication.translate("MainWindow", u"STOP", None))
        self.runProgressBarLabel.setText(QCoreApplication.translate("MainWindow", u"Run", None))
        self.taskProgressBarLabel.setText(QCoreApplication.translate("MainWindow", u"Task", None))
        self.startMeasurementButton.setText(QCoreApplication.translate("MainWindow", u"START", None))
        self.label.setText("")
        self.label_2.setText("")
        self.nEventsLabel.setText(QCoreApplication.translate("MainWindow", u"nEvents", None))
        self.latencyLabel.setText(QCoreApplication.translate("MainWindow", u"Latency", None))
        self.thresholdLabel.setText(QCoreApplication.translate("MainWindow", u"Threshold", None))
        self.extTriggerCheckBox.setText(QCoreApplication.translate("MainWindow", u"Ext Trigger", None))
        self.consoleOutput.setPlaceholderText("")
        self.operatorLabel.setText(QCoreApplication.translate("MainWindow", u"Operator", None))
        self.locationLabel.setText(QCoreApplication.translate("MainWindow", u"Location", None))
        self.addOperatorButton.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.dbStatusLabel_2.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.dbStatusDisplayLabel.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.GiphtIcon.setText("")
        self.runNumberLabel.setText(QCoreApplication.translate("MainWindow", u"Run:", None))
        self.slotsLabel.setText(QCoreApplication.translate("MainWindow", u"Slots", None))
        self.runNumberLabelValue.setText("")
        self.configureSlotsButton.setText(QCoreApplication.translate("MainWindow", u"Configure Slots", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.MeasurementTab), QCoreApplication.translate("MainWindow", u"Measurement", None))
        self.convertAllResultsButton.setText(QCoreApplication.translate("MainWindow", u"Convert all", None))
        self.uploadAllResultsButton.setText(QCoreApplication.translate("MainWindow", u"Upload all", None))
        self.loadResultFolderButton.setText(QCoreApplication.translate("MainWindow", u"Load result folder", None))
        self.uploadResultsButton.setText(QCoreApplication.translate("MainWindow", u"Upload", None))
        self.convertResultsButton.setText(QCoreApplication.translate("MainWindow", u"Convert", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.ResultsTab), QCoreApplication.translate("MainWindow", u"Results", None))
        self.ph2AcfLabel_2.setText(QCoreApplication.translate("MainWindow", u"Device Package", None))
        self.openDialogPh2ACFFolderButton.setText("")
        self.checkPh2Acf.setText(QCoreApplication.translate("MainWindow", u"Ph2_ACF not res-\n"
" ponsive Check", None))
        self.openDefault2SXmlButton.setText(QCoreApplication.translate("MainWindow", u"Open", None))
        self.openDialogResultsFolderButton.setText("")
        self.openDialogDevicePackageFolderButton.setText("")
        self.defaultPSXmlLabel.setText(QCoreApplication.translate("MainWindow", u"Default PS XML", None))
        self.checkControlhub.setText(QCoreApplication.translate("MainWindow", u"Controlhub OFF \n"
" Check", None))
        self.openDialogDefault2SXmlFileButton.setText("")
        self.restartDeviceButton.setText(QCoreApplication.translate("MainWindow", u"Restart Devices", None))
        self.defaultPSXmlLineEdit.setText("")
        self.openDialogDefaultPSXmlFileButton.setText("")
        self.ph2AcfLabel.setText(QCoreApplication.translate("MainWindow", u"Ph2_ACF Folder", None))
        self.default2SXmlLabel.setText(QCoreApplication.translate("MainWindow", u"Default 2S XML", None))
        self.ph2AcfFolderLineEdit.setText("")
        self.invertPlotCheckBox.setText(QCoreApplication.translate("MainWindow", u"Invert IV plot", None))
        self.powerSupplyDialogCheckBox.setText(QCoreApplication.translate("MainWindow", u"Power supply dialogs", None))
        self.showPlotCheckBox.setText(QCoreApplication.translate("MainWindow", u"Show IV plot", None))
        self.groupBoxLpGbt.setTitle("")
        self.lpGBTv0RadioButton.setText(QCoreApplication.translate("MainWindow", u"lpGBTv0", None))
        self.lpGBTv1RadioButton.setText(QCoreApplication.translate("MainWindow", u"lpGBTv1", None))
        self.autoCheckStartCheckBox.setText(QCoreApplication.translate("MainWindow", u"Check devices on startup  ", None))
        self.groupBoxCic.setTitle("")
        self.cic2RadioButton.setText(QCoreApplication.translate("MainWindow", u"CIC2", None))
        self.cic1RadioButton.setText(QCoreApplication.translate("MainWindow", u"CIC1", None))
        self.resultFolderLineEdit.setText("")
        self.default2SXmlLineEdit.setText("")
        self.resultsFolderLabel.setText(QCoreApplication.translate("MainWindow", u"Results folder", None))
        self.devicePackageFolderLineEdit.setText("")
        self.openDefaultPSXmlButton.setText(QCoreApplication.translate("MainWindow", u"Open", None))
        self.startControlhubButton.setText(QCoreApplication.translate("MainWindow", u"Start Controlhub", None))
        self.addFc7Button.setText(QCoreApplication.translate("MainWindow", u"Add FC7", None))
        self.removeDeviceButton.setText(QCoreApplication.translate("MainWindow", u"Remove Device", None))
        self.addPowerSupplyButton.setText(QCoreApplication.translate("MainWindow", u"Add PowerSupply", None))
        self.addArduinoButton.setText(QCoreApplication.translate("MainWindow", u"Add Arduino", None))
        self.GiphtIcon_2.setText("")
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.ConfigurationTab), QCoreApplication.translate("MainWindow", u"Configuration", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.MonitorTab), QCoreApplication.translate("MainWindow", u"Monitor", None))
        self.baserUrlLabel.setText(QCoreApplication.translate("MainWindow", u"Base URL", None))
        self.dbStatusDisplayLabel_2.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.dbStatusLabelName.setText(QCoreApplication.translate("MainWindow", u"DB Status", None))
        self.initDbHandlerOnStartupCheckBox.setText("")
        self.useDbToCheckModules.setText(QCoreApplication.translate("MainWindow", u"Use DB to check Modules", None))
        self.useDbToCheckModulesButton.setText("")
        self.uploadUrlLabel.setText(QCoreApplication.translate("MainWindow", u"Upload URL", None))
        self.initDbStartupLabel.setText(QCoreApplication.translate("MainWindow", u"Init DB Wrapper on Startup", None))
        self.checkDbButton.setText(QCoreApplication.translate("MainWindow", u"Check", None))
        self.testButton.setText(QCoreApplication.translate("MainWindow", u"Test", None))
        self.expertModeCheckBox.setText(QCoreApplication.translate("MainWindow", u"Expert Mode", None))
        self.ivReadoutsLabel_3.setText(QCoreApplication.translate("MainWindow", u"IV Voltage step", None))
        self.vSettingsLabel_2.setText(QCoreApplication.translate("MainWindow", u"IV VTRX+ Light OFF", None))
        self.ivReadoutsLabel_4.setText(QCoreApplication.translate("MainWindow", u"IV Max Voltage (unencapsulated)", None))
        self.ivReadoutsLabel.setText(QCoreApplication.translate("MainWindow", u"IV Readouts", None))
        self.ivReadoutsLabel_5.setText(QCoreApplication.translate("MainWindow", u"IV Max Voltage (encapsulated)", None))
        self.ivReadoutsLabel_2.setText(QCoreApplication.translate("MainWindow", u"IV Settling Time (ms)", None))
        self.HVTestLabel.setText(QCoreApplication.translate("MainWindow", u"Module Test HV (unencapsulated)", None))
        self.vtrxLightOffCheckBox.setText("")
        self.LVTestLabel.setText(QCoreApplication.translate("MainWindow", u"Module Test LV", None))
        self.HVTestLabel_2.setText(QCoreApplication.translate("MainWindow", u"Module Test HV (encapsulated)", None))
        self.settlingTimeLineEdit.setText(QCoreApplication.translate("MainWindow", u"SettlingTime", None))
        self.vStepLineEdit.setText(QCoreApplication.translate("MainWindow", u"V_Step", None))
        self.vMaxUnencapsulatedLineEdit.setText(QCoreApplication.translate("MainWindow", u"V_Max", None))
        self.vMaxUnencapsulatedLineEdit.setPlaceholderText("")
        self.vMaxEncapsulatedLineEdit.setText(QCoreApplication.translate("MainWindow", u"V_Max", None))
        self.vMaxEncapsulatedLineEdit.setPlaceholderText("")
        self.LVModuleTestLineEdit.setText(QCoreApplication.translate("MainWindow", u"10.5", None))
        self.HVModuleTestUnencapsulatedLineEdit.setText(QCoreApplication.translate("MainWindow", u"-300", None))
        self.HVModuleTestEncapsulatedLineEdit.setText(QCoreApplication.translate("MainWindow", u"-300", None))
        self.centralTabWidget.setTabText(self.centralTabWidget.indexOf(self.ExpertTab), QCoreApplication.translate("MainWindow", u"Expert Tab", None))
    # retranslateUi

